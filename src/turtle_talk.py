#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from geometry_msgs.msg import Twist

rospy.init_node('teleop_turtle')
pub = rospy.Publisher('/turtle1/cmd_vel',Twist,queue_size=10)

while not rospy.is_shutdown():
    vel = Twist()
    direction = raw_input('進む方向を前，後，左，右から選んでください>')
    if '前' in direction:
        vel.linear.x = 0.5
    if '後' in direction:
        vel.linear.x = -0.5
    if '左' in direction:
        vel.angular.z = 1.0
    if '右' in direction:
        vel.angular.z = -1.0
    pub.publish(vel)