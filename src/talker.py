#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import String

rospy.init_node('talker')
pub = rospy.Publisher('chatter',String,queue_size=10) #'chatter'という名前のPublisher

while not rospy.is_shutdown():
    fruit = raw_input()
    pub.publish(fruit)  