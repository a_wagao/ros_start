#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import String

def callback(message):
    mydic = {'りんご':'赤','ぶどう':'紫','バナナ':'黄','みかん':'オレンジ','もも':'ピンク'}
    color = mydic[message.data] 
    print color
    #これ以降にcallback関数内の処理を記述．
    
rospy.init_node('listener')
sub = rospy.Subscriber('chatter',String,callback)
rospy.spin()